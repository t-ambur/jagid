How to use the JagID server:

open your bash/git terminal and start the server:
node server.js

You can optionally provide a port number:
node server.js 8000

The default port is 7158, which was the closest to the keys j a g i on my keyboard

With the server running, you need to open whatever port you selected.

Then you can make get and post requests to the server using your ip address

To save a file:
open post.py
change the ENDPOINT to http://IP/upload
go to your terminal and enter:
python post.py FILENAME

The terminal window that is running the server will say that it is receiving a file and give its details.
When the file transfer is done, the server will close the connection and print 'done'

To retrieve a file:
open get.py
change the ENDPOINT to http://IP/get
go to your terminal and enter:
python get.py
or
python get.py FILENAME

The terminal window that is running the server will say file requested... Done
The file will appear in the same directory as get.py


-------------
Ultimately, both get.py and post.py can be integrated into the same python files as the machine learning files,
Integrate the code for post.py into the end of the training file, so that the trained files can be stored on the server.
Integrate the code for get.py into the analyze file, so that it can grab the file from the server

--------

TO DO:
------
Have the machine learning file see if the app user is loaded in the SQL database
Save the files posted based on username
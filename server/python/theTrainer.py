import cv2
import os
import sys
import requests
import numpy as np
from PIL import Image
import pickle

BASE_DIR = os.path.dirname(os.path.abspath(__file__)) #file pathname
#image_dir = os.path.join(BASE_DIR, "images") #goes into images folder
data = "false"
# sys.argv[2] is the directory containing the image files, sys.argv[1] is the location to save to on the server
if len(sys.argv) < 3:
    print(data)
    sys.stdout.flush()
    exit(1)
image_dir = sys.argv[2]
STORE_DIR = sys.argv[1]
# EXAMPLE, this is what the paths look like when i run the server
# sys.argv[1] = D:\\SE\\jagid\\server/files/1/store
# sys.argv[2] = D:\\SE\\jagid\\server/files/1/training"

# This is the name of the file to be produced
OUTPUT_FILE = "trainer.yml"
OUTPUT_LOCATION = os.path.join(STORE_DIR, OUTPUT_FILE)
CASCADE_LOCATION = BASE_DIR + "\\haarcascade_frontalface_alt2.xml"
face_cascade = cv2.CascadeClassifier(CASCADE_LOCATION)

# Our variable we are "training" on faces - utilizes cv2 lib

recognizer = cv2.face_LBPHFaceRecognizer.create()

current_id = 0 #used to "label" each of the profile folder labels - designate who is who
label_ids = {} #empty dictionary
x_train = [] #pixel values
y_labels = [] #profile folder label numbers

#if os.path.isdir(image_dir):
#    print("true")
#else:
#    print("false")
#
#if os.path.isdir(STORE_DIR):
#    print("true")
#else:
#    print(STORE_DIR)

# cycles through designated folder(above) finds png and jpg files and returns pathnames
for root, dirs, files in os.walk(image_dir):
    for file in files:
        if file.endswith("png") or file.endswith("jpg"):
            path = os.path.join(root, file)
            # this returns the FOLDER name
            # NEED to have each profile have saved pics in designated folder for each PERSON
            label = os.path.basename(root).replace(" ", "-").lower()

            #print(label, path)
            if not label in label_ids:
                label_ids[label] = current_id
                current_id += 1
            id_ = label_ids[label]
            #print(label_ids)

            pil_image = Image.open(path).convert("L") # converts to grayscale
            size = (550, 550)
            final_image = pil_image.resize(size, Image.ANTIALIAS) #P laceholder image is resized and used in training
            image_array = np.array(final_image, "uint8") # converts image to numpy array
            #print(image_array) # transforms every image into arrays with pixel values

            # Same as on faceRecognition.py but reads from the saved images rather than the live image
            faces = face_cascade.detectMultiScale(image_array, scaleFactor=1.5, minNeighbors=5)

            for(x, y, w, h) in faces:
                roi = image_array[y:y+h, x:x+w]
                x_train.append(roi) #numbers for pixel values
                y_labels.append(id_) #label numbers for profile folders

# print(y_labels)
# print(x_train)

# saves all profile folder label ID's for training
with open("labels.pickle", 'wb') as f:
    pickle.dump(label_ids, f)

recognizer.train(x_train, np.array(y_labels))
#recognizer.save(OUTPUT_FILE) #saves training data, local
##############################################
# save file on server
recognizer.save(OUTPUT_LOCATION)
# tell node the training was okay
data = "true"
print(data)
sys.stdout.flush()
exit()

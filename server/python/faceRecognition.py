import numpy as np
import cv2
import sys
import os
import requests
import pickle
import time

data = "false"

BASE_DIR = os.path.dirname(os.path.abspath(__file__)) #file pathname
CASCADE_LOCATION = BASE_DIR + "\\haarcascade_frontalface_alt2.xml"
face_cascade = cv2.CascadeClassifier(CASCADE_LOCATION)

start = time.time()
TIME_TIL_DEATH = 5

REQUESTED_FILE = "trainer.yml"
# sys.argv[1] is where trainer.yml was stored after theTrainer.py completes, sys.argv[2] contains the file to verify
STORED_DIR = sys.argv[1]
VERIFY_DIR = sys.argv[2]
USERNAME = sys.argv[3]

INPUT_LOCATION = os.path.join(STORED_DIR, REQUESTED_FILE)
VERIFY_LOCATION = os.path.join(VERIFY_DIR, "vid.mp4") # the video file needs to be named this

#open(INPUT_LOCATION, 'wb').write(r.content)

#if os.path.isfile(VERIFY_LOCATION):
#    print("true")
#else:
#    print("false")

# Establishes recognizer and reads data made in faces-train
recognizer = cv2.face_LBPHFaceRecognizer.create()
#print(INPUT_LOCATION)
recognizer.read(INPUT_LOCATION)

# loads in the profile ID names
labels = {"person_name": 1}
with open("labels.pickle", 'rb') as f:
    og_labels = pickle.load(f)
    labels = {v:k for k,v in og_labels.items()}

#cap = cv2.VideoCapture(0)
cap = cv2.VideoCapture(VERIFY_LOCATION)
while True:
    # This goes one frame at a time
    ret, frame = cap.read()

    # converts frame to grayscale - necessary for CV to work

    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    faces = face_cascade.detectMultiScale(gray, scaleFactor=1.5, minNeighbors=5) # mess with these values to increase accuracy
    # iterates through faces
    for(x,y,w,h) in faces:
        # print(x, y, w, h) #numbers displayed relay to facial recognition values - no face = numbers stop
        # Provides a REGION OF INTEREST (ROI) value
        roi_gray = gray[y:y+h, x:x+w] # Our ROI has these coordinates (can alter but idk man)\\We are displaying in color but READING in gray hence roi_gray
        roi_color = frame[y:y+h, x:x+w] # Reads roi of color frame (not really necessary but w/e

        # Can alter conf range to dial in what we need.
        # Uses nupy generated matrix of frame taken from video in roi_gray to compare to trained recognizer data
        # NOTE: COMMENT OUT PRINT LINE AFTER ABOVE FOR STATEMENT (currently line 22) TO JUST GET THE PROFILE ID # OF MATCHED FACE
        id_, conf = recognizer.predict(roi_gray)
        if conf >= 45 and conf <= 85:
            if labels[id_] == USERNAME:
                data = "true"
            #data = "true"
            #print(id_)
            #print(USERNAME)
            #print(labels[id_]) # gives us the actual profile name (folder name)
        img_item = "my-image.png" # saves image
        cv2.imwrite(img_item, roi_gray) # This line and the line above saves an image of JUST the face which is of the coordinates above
        # alter line above to save MULTIPLE images (it just saves one at the moment)

        # This block of code creates a box around our ROI so we can visually monitor our capture
        color = (255, 0, 0) # Blue Green Red 0-255 color of our box
        stroke = 2 # how thicc our line will be
        end_coord_x = x+w
        end_coord_y = y+h
        cv2.rectangle(frame, (x, y), (end_coord_x, end_coord_y), (0, 255, 0), 1)

    # Displays frame
    #cv2.imshow('frame', frame)
    # Kills the script/video read if "q" is hit or after 5 seconds.
    # Change TIME_TIL_DEATH value (its in seconds) to make it longer/shorter
    if (time.time() > start + TIME_TIL_DEATH) or (cv2.waitKey(20) & 0xFF == ord('q')):
        break



# Released what was captured after process is completed
cap.release()
cv2.destroyAllWindows()

#####################
# Whatever you print will be interpreted by the server as accepted or denied
# result = "false"
print(data)
sys.stdout.flush()

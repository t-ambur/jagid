import time
import pygame
pygame.mixer.init()
#Code by Jacob Della
#Plays a variety of sound effects based on the user auth state.
#Fairly easy and straight forward to customize
#Used VOX because it was too much fun not to do
amigo = pygame.mixer.Sound("audio/amigo.wav")
day = pygame.mixer.Sound("audio/day.wav")
a = pygame.mixer.Sound("audio/a.wav")
nice = pygame.mixer.Sound("audio/nice.wav")
access = pygame.mixer.Sound("audio/access.wav")
alarm = pygame.mixer.Sound("audio/alarm.wav")
have = pygame.mixer.Sound("audio/have.wav")
bell = pygame.mixer.Sound("audio/bell.wav")
denied = pygame.mixer.Sound("audio/denied.wav")
engaged = pygame.mixer.Sound("audio/engaged.wav")
bigwarning = pygame.mixer.Sound("audio/bigwarning.wav")
granted = pygame.mixer.Sound("audio/granted.wav")
unauthorized  = pygame.mixer.Sound("audio/unauthorized.wav")
user  = pygame.mixer.Sound("audio/user.wav")
lockout  = pygame.mixer.Sound("audio/lockout.wav")

def blacklistalarmsound():
    print("Access Denied, Alarm Engaged")
    access.play()
    time.sleep(1.5)
    denied.play()
    time.sleep(1.2)
    lockout.play()
    time.sleep(1.2)
    engaged.play()
    time.sleep(1.2)
    alarm.play()
    time.sleep(1.2)
    engaged.play()
    time.sleep(1.25)
    bigwarning.play()
    time.sleep(1.25)
    bigwarning.play()
    time.sleep(1.25)
    bigwarning.play()
    time.sleep(1.25)
    bigwarning.play()

def accessgrantedsound():
    print("Access Granted")
    access.play()
    time.sleep(1.5)
    granted.play()

#Stupid easter egg :)
def developeraccessgrantedsound():
    print("Access Granted Amigo")
    access.play()
    time.sleep(1.5)
    granted.play()
    time.sleep(1.3)
    amigo.play()
    time.sleep(1)
    have.play()
    time.sleep(1)
    a.play()
    time.sleep(1)
    nice.play()
    time.sleep(1)
    day.play()
    time.sleep(1)

        



import cv2
import os
import numpy as np
from PIL import Image
import pickle

BASE_DIR = os.path.dirname(os.path.abspath(__file__)) #file pathname
image_dir = os.path.join(BASE_DIR, "images") #goes into images folder

face_cascade = cv2.CascadeClassifier('cascades/data/haarcascade_frontalface_alt2.xml')

# Our variable we are "training" on faces - utilizes cv2 lib

recognizer = cv2.face_LBPHFaceRecognizer.create()

current_id = 0 #used to "label" each of the profile folder labels - designate who is who
label_ids = {} #empty dictionary
x_train = [] #pixel values
y_labels = [] #profile folder label numbers

# cycles through designated folder(above) finds png and jpg files and returns pathnames
for root, dirs, files in os.walk(image_dir):
    for file in files:
        if file.endswith("png") or file.endswith("jpg"):
            path = os.path.join(root, file)
            # this returns the FOLDER name
            # NEED to have each profile have saved pics in designated folder for each PERSON
            label = os.path.basename(root).replace(" ", "-").lower()

            print(label, path)
            if not label in label_ids:
                label_ids[label] = current_id
                current_id += 1
            id_ = label_ids[label]
            print(label_ids)

            pil_image = Image.open(path).convert("L") # converts to grayscale
            size = (550, 550)
            final_image = pil_image.resize(size, Image.ANTIALIAS) #P laceholder image is resized and used in training
            image_array = np.array(final_image, "uint8") # converts image to numpy array
            print(image_array) # transforms every image into arrays with pixel values

            # Same as on faceRecognition.py but reads from the saved images rather than the live image
            faces = face_cascade.detectMultiScale(image_array, scaleFactor=1.5, minNeighbors=5)

            for(x, y, w, h) in faces:
                roi = image_array[y:y+h, x:x+w]
                x_train.append(roi) #numbers for pixel values
                y_labels.append(id_) #label numbers for profile folders

# print(y_labels)
# print(x_train)

# saves all profile folder label ID's for training
with open("labels.pickle", 'wb') as f:
    pickle.dump(label_ids, f)

recognizer.train(x_train, np.array(y_labels))
recognizer.save("trainer.yml") #saves training data



